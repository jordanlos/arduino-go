#include <Arduino.h>
#include <stdint.h>
#include "pathfinder.h"
#include "board.h"

using namespace std;

//extern intersection board[81];
void indexToRC(position* pos, int i){
	pos->index = i;
	pos->r = i / 9;
	pos->c = i % 9;
}	

// Ensures all intersections on the board are nuetral
void createBoard( intersection* newBoard){
	for( int i = 0; i < 9; i++){
		for( int j = 0; j < 9; j++){
			int currIntersection = i*9 + j;
			newBoard[ currIntersection ].r = i;
			newBoard[ currIntersection ].c = j;
			newBoard[ currIntersection ].occupiedBy = 0;
		}
	}
}

// Helper function to update positions from r,c, to index
void updatePosition( position * currentPosition, int16_t newr, int16_t newc ) {
	currentPosition->index = newr*9 + newc ;
	currentPosition->r = newr;
	currentPosition->c = newc;
}

// Prints a text board to the Serial monitor - largely for debugging
void printBoard( intersection* boardState){
	for( int i = 0; i < 9; i++){
		for( int j = 0; j < 9; j++){
			int currIntersection = i*9 + j;	
			if( boardState[ currIntersection ].occupiedBy == -1 ){
				Serial.print("B ");
			} else if ( boardState[ currIntersection ].occupiedBy == 1 ){
				Serial.print("W ");
			} else {
				Serial.print("- ");
			}
			if( j == 8 ){
				Serial.println();
			}
		}
	}
}

// Prints board for testing
void printTestBoard( ) {
	intersection testBoard1[81];
	createBoard( testBoard1 );

	// Black
	testBoard1[3].occupiedBy = -1;
	testBoard1[4].occupiedBy = -1;
	testBoard1[12].occupiedBy = -1;
	testBoard1[20].occupiedBy = -1;
	testBoard1[21].occupiedBy = -1;
	testBoard1[29].occupiedBy = -1;
	testBoard1[31].occupiedBy = -1;
	testBoard1[39].occupiedBy = -1;
	testBoard1[47].occupiedBy = -1;
	testBoard1[49].occupiedBy = -1;
	testBoard1[57].occupiedBy = -1;
	testBoard1[58].occupiedBy = -1;
	testBoard1[66].occupiedBy = -1;
	testBoard1[75].occupiedBy = -1;

	// White
	//	testBoard1[05].occupiedBy = 1;
	testBoard1[5].occupiedBy = 1;
	testBoard1[13].occupiedBy = 1;
	testBoard1[22].occupiedBy = 1;
	testBoard1[23].occupiedBy = 1;
	testBoard1[32].occupiedBy = 1;
	testBoard1[29].occupiedBy = 1;
	testBoard1[40].occupiedBy = 1;
	testBoard1[41].occupiedBy = 1;
	testBoard1[50].occupiedBy = 1;
	testBoard1[59].occupiedBy = 1;
	testBoard1[61].occupiedBy = 1;
	testBoard1[67].occupiedBy = 1;
	testBoard1[68].occupiedBy = 1;
	testBoard1[77].occupiedBy = 1;

	Serial.println();
	Serial.println("Test Board 1 ");
	printBoard( testBoard1 );
	Serial.println("Test Over ");

	position testPosition;
	testPosition.r = 0;
	testPosition.c = 8;
	testPosition.index = 8;

	Serial.println(sizeof(testBoard1));
	Serial.println(sizeof(testPosition));
	//capture(testBoard1, &testPosition ,1, 0, 1, -1 ); 
	Serial.println("Test Board after capture ");
	printBoard( testBoard1 );
	Serial.println("Test Over ");
}
/*
void printTestBoard1() {
	// Black
	board[10].occupiedBy = -1;
	board[19].occupiedBy = -1;
	board[28].occupiedBy = -1;

	// White
	board[0].occupiedBy = 1;
	board[9].occupiedBy = 1;
	board[18].occupiedBy = 1;
	board[27].occupiedBy = 1;
	board[37].occupiedBy = 1;
	board[2].occupiedBy = 1;
	board[11].occupiedBy = 1;
	board[20].occupiedBy = 1;
	board[29].occupiedBy = 1;
	board[1].occupiedBy = 1;
	

//	cout << endl << "Test Board 1 " << endl;
	printBoard( );
//	cout << "Test over  " << endl;

//	cout << "test pathfinder" << endl;
	position testPosition;
	updatePosition( &testPosition, 1, 1 );
	capture( &testPosition ,1, -1, 0, 0 ); 
//	cout << endl << "Test Board after pathfind " << endl;
	printBoard( );
//	cout << "Test over  " << endl;
}

			
void printTestBoard2() {
	// Black
	board[10].occupiedBy = -1;
	board[19].occupiedBy = -1;
	board[28].occupiedBy = -1;

	// White
	board[0].occupiedBy = 1;
	board[9].occupiedBy = 1;
	board[18].occupiedBy = 1;
	board[27].occupiedBy = 1;
	board[2].occupiedBy = 1;
	board[11].occupiedBy = 1;
	board[20].occupiedBy = 1;
	board[29].occupiedBy = 1;
	board[1].occupiedBy = 1;
	

//	cout << endl << "Test Board 1 " << endl;
	printBoard( );
//	cout << "Test over  " << endl;

//	cout << "test pathfinder" << endl;
	position testPosition;
	updatePosition( &testPosition, 1, 1 );
	capture( &testPosition ,1, -1, 0, 0 ); 
//	cout << endl << "Test Board after pathfind " << endl;
	printBoard( );
//	cout << "Test over  " << endl;
}
	
*/

