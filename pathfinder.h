#ifndef _PATHFINDER_H_
#define _PATHFINDER_H_

#include <Arduino.h>
#include <stdint.h>

struct intersection {
	// r = row; c = column;
	uint16_t r, c;
	// -1 = Black, 0=Neutral, 1=White
	int16_t occupiedBy;
};

struct position {
	int16_t index, r, c;
};

struct player {
	int color;
	int pathCondition;
	int terminator;
	int modifier;
	bool hasChain;
	int points;
};

void createBoard( intersection* board);
void updatePosition( position * currentPosition, int16_t newr, int16_t newc );
void findPath( intersection* boardInPlay, position* p0, player* Player, bool* isCounted );
void capture( intersection* boardInPlay, position* p0, player* Player);
void printBoard( intersection* board );
void printTestBoard( );
void indexToRC(position* pos, int i);

#endif
