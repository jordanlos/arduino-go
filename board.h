#ifndef _BOARD_H_
#define _BOARD_H_

#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <TouchScreen.h>
#include <SPI.h>
#include "pathfinder.h"


#define TFT_DC 9
#define TFT_CS 10

struct cursorPosition {
	int16_t r, c, x, y;
};
extern Adafruit_ILI9341 tft;


void drawStart(bool& player);

void drawPieceInBoard(int16_t r, int16_t c, int16_t x, int16_t y, int16_t color);

void setupBoard();
void drawBoard();
/*
void drawBlack(uint16_t x, uint16_t y);
void drawWhite(uint16_t x, uint16_t y);
*/
int16_t r2x(int16_t x);
int16_t x2r(int16_t r);
int16_t moveCursor( cursorPosition* cursor, intersection* board,player* Black, player* White, bool* turn, int& pass);

//void drawPiece(uint16_t x, uint16_t y, uint16_t color);
void updateBoard(intersection* board, player* Black, player* White,uint16_t cursorIndex, bool& turn);
void pass( );

void drawPass( bool touched );
int passButton( bool* turn);
void endGame( intersection* board);



#endif
