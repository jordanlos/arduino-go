#include "board.h"
#include "pathfinder.h"

#define TFT_WIDTH 320
#define TFT_HEIGHT 240

#define YP A2
#define XM A3
#define YM 5
#define XP 4

#define TS_MINX 150
#define TS_MINY 120
#define TS_MAXX 920
#define TS_MAXY 940

#define MINPRESSURE   10
#define MAXPRESSURE 1000

#define JOY_VERT_ANALOG A1
#define JOY_HORIZ_ANALOG A0
#define JOY_SEL 2

#define JOY_DEADZONE 64
#define JOY_CENTRE 512

#define MARGIN 16
#define INTERSECTION_SIZE 26
#define BOARD_CENTRE_Y 120
#define BOARD_CENTRE_X 160
#define BOARD_BACKGROUND 0x92C2
#define BOARD_LINE 0xCD4A

#define SINGLE_ARD_DEBUG 1 //so game works on single arduino, 0 to disable

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

void setupBoard() {
	init();

	pinMode(JOY_SEL, INPUT_PULLUP);

	Serial.begin(9600);
	Serial3.begin(9600);

	tft.begin();

	tft.setRotation(-1);
	tft.setTextWrap(false);
}

void drawPieceInBoard(int16_t r, int16_t c, int16_t x, int16_t y, int16_t color){
	if( color == -1 ){
		tft.fillCircle(x,y,8,ILI9341_BLACK);
	} else if( color == 1) {
		tft.fillCircle(x,y,9,ILI9341_WHITE);
	} else if(r < 8 && r > 0 && c <8 && c > 0){ // Rm Piece from middle of board
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x-12,y,24,2,BOARD_LINE);
		tft.fillRect(x,y-12,2,24,BOARD_LINE);

	} else if(c == 8 && r != 8 && r != 0){ // right side
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x,y-12,2,24,BOARD_LINE);
		tft.fillRect(x-12,y,12,2,BOARD_LINE);

	} else if (r == 8 && c == 8){ // bottom right corner
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x,y-12,2,12,BOARD_LINE);
		tft.fillRect(x-12,y,14,2,BOARD_LINE);
	} else if (c == 8 && r == 0){ // top right corner
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x,y,2,12,BOARD_LINE);
		tft.fillRect(x-12,y,12,2,BOARD_LINE);

	} else if(c == 0 && r != 8 && r != 0){ // left side
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x,y-12,2,24,BOARD_LINE);
		tft.fillRect(x,y,12,2,BOARD_LINE);

	} else if (c == 0 && r == 8){ //bottom left corner
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x,y-12,2,12,BOARD_LINE);
		tft.fillRect(x,y,12,2,BOARD_LINE);
	} else if (r == 0 && c == 0){ //top left corner
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x,y,2,12,BOARD_LINE);
		tft.fillRect(x,y,12,2,BOARD_LINE);

	} else if(r == 8 && c != 8 && c != 0){ //bottom
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x,y-12,2,12,BOARD_LINE);
		tft.fillRect(x-12,y,24,2,BOARD_LINE);
	} else if(r == 0 && c != 8 && c != 0){ //top
		tft.fillCircle(x,y,11,BOARD_BACKGROUND);
		tft.fillRect(x,y,2,12,BOARD_LINE);
		tft.fillRect(x-12,y,24,2,BOARD_LINE);
	}
}
/*
	dimensions:
	board: 208x208px
	square: 26x26px
	pieces: 20x21 oval (gives illusion of depth)
	cursor: 6x6 circle
*/

// Waits for both arduinos to load
// First person to load is black
// Returns what player you are
void waitOnOpponent(bool &player){

	// The other arduino will only receive "1" if its listening.
	// First arduino to start is the first to listen and first to receive "1"
	Serial3.write(1);
	while (true) {
		if (Serial3.available() > 0 ){
			if (Serial3.read() == 1){
				Serial3.write(2);
				Serial.println("Your Black");
				// This Ardino is Black
				player= true;
				break;
			} else if (Serial3.read() == 2){
				Serial.println("Your white");
				// This Arduino is White
				player= false;
				break;
			}
		}
	}
}

void drawStart(bool& player){
	tft.fillScreen(BOARD_BACKGROUND);
	tft.setTextSize(4);
	tft.setCursor(BOARD_CENTRE_X - 118,BOARD_CENTRE_Y - 18 );
	tft.setTextColor(ILI9341_BLACK);
	tft.print("ARDUINO GO");
	tft.setCursor(BOARD_CENTRE_X - 120,BOARD_CENTRE_Y - 20 );
	tft.setTextColor(BOARD_LINE);
	tft.print("ARDUINO GO");
	tft.setTextSize(2);
	tft.setCursor(BOARD_CENTRE_X - 138,BOARD_CENTRE_Y + 52);
	tft.setTextColor(ILI9341_BLACK);
	tft.print("Click joystick to start!");
	tft.setCursor(BOARD_CENTRE_X - 140,BOARD_CENTRE_Y + 50);
	tft.setTextColor(BOARD_LINE);
	tft.print("Click joystick to start!");
	while (true){
		if( Serial3.available()){
			// White
			Serial.read();
			player = false;
			break;
		} else if (digitalRead(JOY_SEL) == LOW){
			Serial3.write(1);
			player = true;
			break;
		}
	}
}

void drawBoard(){
	tft.fillScreen(0x0000);
	tft.fillRect(0,0,240,240,BOARD_BACKGROUND);
	tft.drawRect(0,0,241,240,ILI9341_BLACK);
	int i;
	for (i = 0;i<9;i++){
		tft.fillRect((16+i*26),16,2,208,BOARD_LINE); //vertical bars
		tft.fillRect(16,(16+i*26),210,2,BOARD_LINE); //horizontal bars
	}
}

// Note - since the margins and intersections are the same, these
// helper functions will work for vertical as well.
int16_t r2x(int16_t r){
	int x= r*INTERSECTION_SIZE + MARGIN;
	return x;
}
// Assumes cursor is only ever on a gridline
int16_t x2r(int16_t x){
	int r= ( x - MARGIN ) / INTERSECTION_SIZE;
	return r;
}

void drawPass(bool touched){
	tft.setTextSize(1);
	if (touched){
		tft.fillRect(266,0,54,40,0xA596);//below draws pass button
		tft.setCursor(282,13);
		tft.setTextColor(ILI9341_BLACK);
		tft.print("Pass");
	} else {
		tft.fillRect(266,0,54,40,0xBA42);//below draws pass button during press
		tft.setCursor(282,13);
		tft.setTextColor(ILI9341_WHITE);
		tft.print("Pass");
	}
}

int passButton( bool* turn){
	TSPoint touch = ts.getPoint();
	// get the y coordinate of where the display was touched
	// remember the x-coordinate of touch is really our y-coordinate
	// on the display
	int touchY = map(touch.x, TS_MINX, TS_MAXX, 0, TFT_HEIGHT - 1);

	// need to invert the x-axis, so reverse the
	// range of the display coordinates
	int touchX = map(touch.y, TS_MINY, TS_MAXY, TFT_WIDTH - 1, 0);

	if (touch.z < MINPRESSURE || touch.z > MAXPRESSURE) {
		if (touchX >= 278 && touchY <= 50){
			*turn = !turn;
			drawPass(1);
			delay(150); //since pass button becomes locked out after touch, simple delay works
			drawPass(0);
			delay(150);
			return 1; // if touched, pass count = 1
		}
	}
	return 0; // otherwise = 0
}
int16_t piecePosX;
int16_t piecePosY;

bool backDraw = 1;

void updateBoard(intersection* board, player* Black, player* White,uint16_t cursorIndex, bool& firstPlayer){
	cursorPosition cursor;
	cursor.r = cursorIndex / 9;
	cursor.c = cursorIndex % 9;
	cursor.x = r2x( cursor.c );
	cursor.y = r2x( cursor.r);

	int16_t boardPosition = cursor.r*9 + cursor.c;

	position down;
	down.c = cursor.c;
	down.r = cursor.r + 1;
	updatePosition( &down, down.r, down.c);

	position up;
	up.c = cursor.c;
	up.r = cursor.r - 1;
	updatePosition( &up, up.r, up.c);

	position right;
	right.c = cursor.c + 1;
	right.r = cursor.r;
	updatePosition( &right, right.r, right.c);

	position left;
	left.c = cursor.c -1;
	left.r = cursor.r;
	updatePosition( &left, left.r, left.c);
	// Draw Circle if Joystick clicks
	// Also passes turn
	if (firstPlayer){ //turn 0 is black
		drawPieceInBoard(cursor.r,cursor.c,cursor.x,cursor.y, -1);
		board[ boardPosition ].occupiedBy = -1; //flag position as occupied by black

		Black->color = -1 ;
		Black->pathCondition = 1 ;
		Black->terminator = 0;
		Black->modifier = 0;

		capture( board, &down, Black); //run capture algorithm
		capture( board, &up, Black);
		capture( board, &right, Black);
		capture( board, &left, Black);
	} else { //turn 1 is white
		drawPieceInBoard(cursor.r,cursor.c,cursor.x,cursor.y, 1);
		board[ boardPosition ].occupiedBy = 1;
		Serial.println("White Piece");

		White->color = 1 ;
		White->pathCondition = -1 ;
		White->terminator = 0;
		White->modifier = 0;

		capture( board, &down, White);
		capture( board, &up, White);
		capture( board, &right, White);
		capture( board, &left, White);
	}
}

//recursively skips over pieces, unless it hits the edge
void skiPiece(cursorPosition* cursor, intersection* board,int16_t oldr,int16_t oldc, int16_t boardPosition){
	//skips over if spot is occupied and is a new move
	if (board[ boardPosition ].occupiedBy != 0 && (cursor->r != oldr || cursor->c != oldc)){
		if (cursor->r > oldr){
			cursor->r+=1;
		} else if (cursor->r < oldr){
			cursor->r-=1;
		} else if (cursor->c > oldc){
			cursor->c+=1;
		} else if (cursor->c < oldc){
			cursor->c-=1;
		}

		//if trying to skip over hits the edge of the board, do not move cursor
		if (cursor->r > 8 || cursor->r < 0 || cursor->c > 8 || cursor->c < 0){
			cursor->r = oldr;
			cursor->c = oldc;
		}
		boardPosition = cursor->r*9 + cursor->c; // update board position
		skiPiece(cursor,board,oldr,oldc,boardPosition);
	}
}


// number of turns taken so far
int16_t moveCursor( cursorPosition* cursor, intersection* board, player* Black, player* White, bool* turn, int& pass ){
	// Collect Joystick Data
	int v = analogRead(JOY_VERT_ANALOG); //value of vertical joystick movement
	int h = analogRead(JOY_HORIZ_ANALOG); //value of horizontal joystick movement

	// Store existing cursor data
	int16_t oldr = cursor->r;//stores old r val before changing it below
	int16_t oldc = cursor->c;//stores old c val before changing it below
	int16_t oldx = cursor->x;//stores old x val before changing it below
	int16_t oldy = cursor->y;//stores old y val before changing it below

	// Update Cursor Data
	if (v > JOY_CENTRE + JOY_DEADZONE) { //changes cusor row and column
		++cursor->r;
	}	else if (v < JOY_CENTRE - JOY_DEADZONE) {
		--cursor->r;
	} else if (h > JOY_CENTRE + JOY_DEADZONE){
		--cursor->c;
	} else if (h < JOY_CENTRE - JOY_DEADZONE){
		++cursor->c;
	}
	cursor->r = constrain(cursor->r,0,8);
	cursor->c = constrain(cursor->c,0,8);

	// Variables for updating capture
	int16_t boardPosition = cursor->r*9 + cursor->c;

	//Skips over pieces
	skiPiece(cursor,board,oldr,oldc,boardPosition);

	cursor->x = r2x( cursor->c ); //no more changes to r or c, safe to convert to xy coord
	cursor->y = r2x( cursor->r);

	//when joystick clicks down and space is empty, change turns
	if (digitalRead(JOY_SEL) == LOW && board[ boardPosition ].occupiedBy == 0){
		if (turn){ //turn 0 is black
		    *turn = false;
		} else { //turn 1 is white
			*turn = true;
		}

		printBoard( board );//debug, prints virtual board in serial

		backDraw = 0; //this var says either draw or do not draw background in prev pos
		piecePosX = cursor->x; //stores most recent piece position
		piecePosY = cursor->y;
		delay(150); // so you cannot move cursor too fast
		pass = 0; //since two passes in a row ends game, when a new move is completed, reset pass counter to 0
		return boardPosition;
	}

	// Update Board Image to reflact change in cursor
	if (cursor->r != oldr || cursor->c != oldc){
		// tft.fillCircle(cursor->x, cursor->y,3,ILI9341_RED); //cursor
		if (backDraw){ //after placing a piece don't draw background over most recent piece
			tft.fillCircle(oldx,oldy,3,BOARD_BACKGROUND); //redraw BOARD_BACKGROUND

			//redraw grid after leaving spot
			if (oldr < 8 && oldr > 0 && oldc <8 && oldc > 0){
				tft.fillRect(oldx,oldy-3,2,7,BOARD_LINE);
				tft.fillRect(oldx-3,oldy,7,2,BOARD_LINE);

				//border cases
			} else if(oldc == 8 && oldr != 8 && oldr != 0){ // right side
				tft.fillRect(oldx,oldy-3,2,7,BOARD_LINE);
				tft.fillRect(oldx-7,oldy,7,2,BOARD_LINE);

			} else if (oldr == 8 && oldc == 8){ // bottom right corner
				tft.fillRect(oldx,oldy-7,2,7,BOARD_LINE);
				tft.fillRect(oldx-7,oldy,9,2,BOARD_LINE);
			} else if (oldc == 8 && oldr == 0){ // top right corner
				tft.fillRect(oldx,oldy,2,7,BOARD_LINE);
				tft.fillRect(oldx-7,oldy,7,2,BOARD_LINE);

			} else if(oldc == 0 && oldr != 8 && oldr != 0){ // left side
				tft.fillRect(oldx,oldy-3,2,7,BOARD_LINE);
				tft.fillRect(oldx,oldy,7,2,BOARD_LINE);
			} else if (oldc == 0 && oldr == 8){ //bottom left corner
				tft.fillRect(oldx,oldy-7,2,7,BOARD_LINE);
				tft.fillRect(oldx,oldy,7,2,BOARD_LINE);
			} else if (oldr == 0 && oldc == 0){ //top left corner
				tft.fillRect(oldx,oldy,2,7,BOARD_LINE);
				tft.fillRect(oldx,oldy,7,2,BOARD_LINE);
				// x above y below
			} else if(oldr == 8 && oldc != 8 && oldc != 0){ //bottom
				tft.fillRect(oldx,oldy-7,2,7,BOARD_LINE);
				tft.fillRect(oldx-3,oldy,7,2,BOARD_LINE);
			} else if(oldr == 0 && oldc != 8 && oldc != 0){ //top
				tft.fillRect(oldx,oldy,2,7,BOARD_LINE);
				tft.fillRect(oldx-3,oldy,7,2,BOARD_LINE);
			}
		}
		tft.fillCircle(cursor->x, cursor->y,3,ILI9341_RED); //cursor
		delay(150);
	}

	//if cursor position changes, allow background to be drawn again
	if (piecePosX != cursor->x || piecePosY != cursor->y){
		backDraw = 1;
	}
}

void endGame( intersection* board ){
	int blackScore = 0;
	int whiteScore = 0;
	for(int i = 0; i < 81; i++){
		if( board[i].occupiedBy == -1 ){
			blackScore++;
		} else if( board[i].occupiedBy == 1 ){
			whiteScore++;
		}
	}

	tft.fillScreen(0x0000);
	tft.setCursor(BOARD_CENTRE_X - 110,60);
	tft.setTextSize(2);
	tft.setTextColor(ILI9341_WHITE);
	tft.print("BLACK: ");
	tft.print(blackScore);
	tft.print(" White: ");
	tft.println(whiteScore);

	tft.setCursor(BOARD_CENTRE_X - 110,BOARD_CENTRE_Y - 10);
	tft.setTextSize(4);
	tft.print("GAME OVER");
}
