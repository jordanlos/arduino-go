#include "pathfinder.h"
#include "board.h"

extern Adafruit_ILI9341 tft;

#define TFT_DC 9
#define TFT_CS 10
#define TFT_WIDTH 320
#define TFT_HEIGHT 240

#define YP A2
#define XM A3
#define YM 5
#define XP 4

#define TS_MINX 150
#define TS_MINY 120
#define TS_MAXX 920
#define TS_MAXY 940

#define MINPRESSURE   10
#define MAXPRESSURE 1000


#define JOY_VERT_ANALOG A1
#define JOY_HORIZ_ANALOG A0
#define JOY_SEL 2

#define JOY_DEADZONE 64
#define JOY_CENTRE 512

#define BOARD_CENTRE_Y 120
#define BOARD_CENTRE_X 160
#define BOARD_BACKGROUND 0x92C2
#define BOARD_LINE 0xCD4A

// a multimeter reading says there are 300 ohms of resistance across the plate,
// so initialize with this to get more accurate readings

void setup() {
	init();

	pinMode(JOY_SEL, INPUT_PULLUP);

	Serial.begin(9600);
	Serial3.begin(9600);

	tft.begin();

	tft.setRotation(-1);
	tft.setTextWrap(false);
}

// A path is any chain of adjacent intersections occupied by the same property (B/W/N)
// that are, at no point, adjacent to an intersection occupied by a terminating property.
// 1. Position is the struct of the current position
// 2. Player is the player attempting to find a path.
// 3. pathCondition is the state of the path defining the Chain (B,W, or N)
// 4. Terminator is the position that terminates a chain
void pathfind( intersection* boardInPlay, position* p0, player* Player, int* pointsInChain, bool* isCounted){
	// Case 0. The wall - ensure the piece is within boundaries of the board
	if( p0->r > 8 || p0->r < 0 || p0->c > 8 || p0->c < 0 ){
		return;
	}
	// Case 1: The intersection has been counted--Do Nothing.
	if( isCounted[p0->index] == true ){
		return;
	}
	// Declare this intersection counted before calling recursive elements to avoid infinite loop
	isCounted[ p0->index ] = true;

	// Case 2. The intersection is the same as the path condition--Continue the chain
	if( boardInPlay[ p0->index ].occupiedBy == Player->pathCondition  ) {
		pointsInChain[ p0->index ] = 1;

		position up;
		updatePosition( &up, p0->r-1, p0->c);
		pathfind( boardInPlay, &up ,Player, pointsInChain, isCounted);
		// search down
		position down;
		updatePosition( &down, p0->r+1 , p0->c);
		pathfind( boardInPlay, &down ,Player, pointsInChain, isCounted);
		// search right
		position right;
		updatePosition( &right, p0->r, p0->c + 1);
		pathfind( boardInPlay, &right ,Player, pointsInChain, isCounted);
		// search left
		position left;
		updatePosition( &left, p0->r, p0->c - 1);
		pathfind( boardInPlay, &left ,Player, pointsInChain, isCounted);
	} else if( boardInPlay[ p0->index ].occupiedBy == Player->terminator ) {
	// Case 3: The intersection finds a piece terminating the chain
		Player->hasChain = false;
		return;
	} else if( boardInPlay[ p0->index ].occupiedBy == Player->color ) {
		return;
	}

}
// Checks for a path the position and if found captures all pieces in the path.
void capture( intersection* boardInPlay, position* p0, player* Player){
	bool isCounted[81]={false};
	int pointsInChain[81]= {0};
	Player->hasChain= true;

	// Finds the Path
	pathfind( boardInPlay, p0, Player, pointsInChain, isCounted);

	// If a chain was found, captures all the pieces found within the chain
	if( Player->hasChain  ) {
		for( int i = 0; i < 81; i++){
			if (pointsInChain[i] == 1){
				boardInPlay[i].occupiedBy = Player->modifier;
				int16_t x = r2x( boardInPlay[i].c );
				int16_t y = r2x( boardInPlay[i].r );
				drawPieceInBoard(boardInPlay[i].r,boardInPlay[i].c,x,y, Player->modifier);
			}
		}
	}
}


// Checks all empty spaces to see if white or black
// can capture a path of empty spaces.
void endGameCapture( intersection* boardInPlay ){
	Serial.println("Start End Game");
	bool haveChecked[81] = {false};
	player Black, White, Neutral;

	Black.color = -1;
	Black.modifier = -1;
	Black.terminator = 1;
	Black.pathCondition = 0;

	White.color = 1;
	White.modifier = 1;
	White.terminator = -1;
	White.pathCondition = 0;

	for( int i = 0; i < 81; i++){
		if( boardInPlay[i].occupiedBy == 0 ){

				position checkBlack;
				indexToRC( &checkBlack, i );
				capture( boardInPlay, &checkBlack, &Black);

				position checkWhite;
				indexToRC( &checkWhite, i );
				capture( boardInPlay, &checkWhite, &White);
		}
	}
	printBoard( boardInPlay );
	Serial.println("Finish End Game");
}

int main(){
	setupBoard();

	player Black;
	Black.color = -1;
	Black.points = 0; ;

	player White;
	White.color = 1;
	White.points = 0; ;

	// Setup Datastructures for Board
	intersection board[81];
	createBoard( board );


	// Graphics functions for board
	bool firstPlayer;
	drawStart( firstPlayer);
	drawBoard();
	drawPass(0);
	//drawWhite(56,16);
	 //drawBlack(56,42);
	printBoard( board );
	cursorPosition cursor;
	cursor.r = 0;
	cursor.c = 0;
	cursor.x = r2x(0);
	cursor.y = r2x(0);
	tft.fillCircle(cursor.x,cursor.y,3,ILI9341_RED); // inital draw of cursor

	bool inPlay;
	bool otherPlayer;
	if( firstPlayer ){
		inPlay = true;
		otherPlayer = false;
		Serial.println("Im Black");
	} else {
		inPlay = false;
		otherPlayer = true;
		Serial.println("Im White");
	}

	// Hard Code for use below
	bool firstLoop = true;

	int pass = 0;
	int16_t currentPosition;

	// Loop runs until pass is hit twice - which ends the game
	while (pass < 2	){
		Serial.println("Looping");
		// Wait for this arduino to make a move
		while(inPlay){
			currentPosition = moveCursor( &cursor, board, &Black, &White,&inPlay,pass );
			pass += passButton( &inPlay);
		}
		// If pass was played &- should equal 100
		// If pass was not played should eqaul [000,080]
			
		if( pass == 0 ){
			// A white piece was being added to only the white players board.
			// The bug is likely due to the logic of setting up first and second players.
			// Thus, the hard code below prevents the board adding a white piece on the setup.
			if( otherPlayer && firstLoop ){
				firstLoop = false;
			} else {
				updateBoard( board, &Black, &White, currentPosition, firstPlayer );
				Serial.println("board updated while in pLay");
			}
		} else if ( pass >= 2 ){
			// Have to continue to ensure pass=2 is sent, breaking will end the game 
			// only on this arduino
			Serial3.write(200);
			continue;	
		}
		currentPosition += pass*100;
		Serial3.write(currentPosition);
		// Send the position of the played piece
		// Update the board with the piece this arduino played, if this arduino played a piece
		// Wait for 2nd player to move
		while(!inPlay){
			// This call flushes a byte in serial3 that is coming in and calling the if 
			// statement below in advance.
			Serial.println(Serial3.read());

			if (Serial3.available() > 0) {
			Serial.println("Great than 0");
				// Read in the value sent
				uint8_t byteRead = Serial3.read();
				// Value will be 3 digits, 1st digit is the pass value (2 ends game),
				// Second 2 digits is the index for the played cursor
				int passin =   (int)(byteRead) / 100;
				if( passin == 0 ){
					pass = 0;
				} else {
					pass ++;
				}
			
				currentPosition = (int16_t)(byteRead % 100);
				inPlay = true;
				}
		}
		// Only update the board if a new piece was played.
		// A piece is not played if pass = 1.
		if( pass == 0 ){
			updateBoard( board, &Black, &White, currentPosition, otherPlayer );
		}
	}

	endGameCapture( board );
	delay(1000);
	printBoard( board );
	endGame( board );

	Serial.end();
	return 0;
}
