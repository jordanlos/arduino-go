#include <Arduino.h>
//#include <iostream>
#include <stdint.h>

using namespace std;

struct intersection {
	// r = row; c = column;
	uint16_t r, c;
	// -1 = Black, 0=Neutral, 1=White
	int16_t occupiedBy;
} board[81];

void createBoard(){
	for( int i = 0; i < 9; i++){
		for( int j = 0; j < 9; j++){
			int position = i*9 + j;
			board[position].r = i;
			board[position].c = j;
			board[position].occupiedBy = 0;
		}
	}
}

bool isCounted[81] = {false};

struct position {
	int16_t index, r, c;
};

void updatePosition( position * currentPosition, int16_t newr, int16_t newc ) {
	currentPosition->index = newr*9 + newc ;
	currentPosition->r = newr;
	currentPosition->c = newc;
}

// Position is the struct of the current position
// pathCondition is the state of the path defining the Chain (B,W, or N)
// Modifier is the capturing player's color
int i = 0;
bool isChain = true;
void capture( position* p0, int16_t player, int16_t pathCondition, int16_t modifier, int16_t terminator ){
	i++;
	//cout<< "r: " << p0->r << ", c: " << p0->c << ", index: " << p0->index << ", i: " << i<< endl;
	// Case 0: The square has been counted--Do Nothing.
	if( isCounted[p0->index] == true ){
		return;
	}
	// Declare this intersection counted before calling recursive elements to avoid infinite loop
	isCounted[ p0->index ] = true;

	// Case 1: The square is actuall outside the playing board--Do Nothing
	// These define the 4 coditions of a wall, and must be checked after isCounted = true;
	if( p0->r > 8 || p0->r < 0 || p0->c > 8 || p0->c < 0 ){
		return;
	}

	// Case 2. The Piece is same as the players--Do Nothing
	if( board[ p0->index ].occupiedBy == player ) {
		return;
	// Case 3. The intersection is the same as the path condition--Continue the chain
	} else if( board[ p0->index ].occupiedBy == pathCondition ) {
		// search up
		position up;
		updatePosition( &up, p0->r, p0->c - 1);
		capture( &up , player, pathCondition, modifier, terminator);
		// search down + 9
		position down;
		updatePosition( &down, p0->r , p0->c + 1);
		capture( &down , player, pathCondition, modifier, terminator);
		// search right + 1
		position right;
		updatePosition( &right, p0->r + 1, p0->c);
		capture( &right ,  player, pathCondition, modifier, terminator);
		// search left - 1
		position left;
		updatePosition( &left, p0->r - 1, p0->c);
		capture( &left , player, pathCondition, modifier, terminator);
	} else if( board[ p0->index ].occupiedBy == terminator ) {
	// Case 4: The intersection finds a piece terminating the chain
		isChain = false;
	}
	if( isChain ) {
		board[ p0->index ].occupiedBy = modifier;
	}
	/*
	int16_t currentType = board[ p0->index ].occupiedBy;	
	// Case 1. The current position matches the path condition - continue down the path
	if( currentType == pathCondition ){ 
		// search up
		position up;
		updatePosition( &up, p0->index - 9);
		capture( &up , pathCondition, modifier);
		// search down + 9
		position down;
		updatePosition( &down, p0->index + 9);
		capture( &down , pathCondition, modifier);
		// search right + 1
		position right;
		updatePosition( &right, p0->index + 1);
		capture( &right , pathCondition, modifier);
		// search left - 1
		position left;
		updatePosition( &left, p0->index - 1);
		capture( &left , pathCondition, modifier);
	} else if( currentType != pathCondition * -1){ 
	// Case 2. Space is Neutral - take desired action
		board[ p0->index ].occupiedBy = modifier;	
	} else {
	// Case 3. The space is occupied since B=W*-1 - no action
		return;
	}
	*/
};

// Prints a text board to the terminal
void printBoard(){
	for( int i = 0; i < 9; i++){
		for( int j = 0; j < 9; j++){
			int position = i*9 + j;	
			if( board[ position ].occupiedBy == -1 ){
				Serial.print("B ");
			} else if ( board[ position ].occupiedBy == 1 ){
				Serial.print("W ");
			} else {
				Serial.print("- ");
			}
			if( j == 8 ){
				Serial.println();
			}
		}
	}
}

// Prints board for testing
void printTestBoard() {
	// Black
	board[3].occupiedBy = -1;
	board[4].occupiedBy = -1;
	board[12].occupiedBy = -1;
	board[20].occupiedBy = -1;
	board[21].occupiedBy = -1;
	board[29].occupiedBy = -1;
	board[31].occupiedBy = -1;
	board[39].occupiedBy = -1;
	board[47].occupiedBy = -1;
	board[49].occupiedBy = -1;
	board[57].occupiedBy = -1;
	board[58].occupiedBy = -1;
	board[66].occupiedBy = -1;
	board[75].occupiedBy = -1;

	// White
	//	board[05].occupiedBy = 1;
	board[5].occupiedBy = 1;
	board[13].occupiedBy = 1;
	board[22].occupiedBy = 1;
	board[23].occupiedBy = 1;
	board[32].occupiedBy = 1;
	board[29].occupiedBy = 1;
	board[40].occupiedBy = 1;
	board[41].occupiedBy = 1;
	board[50].occupiedBy = 1;
	board[59].occupiedBy = 1;
	board[61].occupiedBy = 1;
	board[67].occupiedBy = 1;
	board[68].occupiedBy = 1;
	board[77].occupiedBy = 1;

	Serial.println();
	Serial.println("Test Board 1 ");
	printBoard( );
	Serial.println("Test Over ");

	position testPosition;
	updatePosition( &testPosition, 0, 8 );
	capture( &testPosition ,1, 0, 1, -1 ); 
	Serial.println("Test Board after capture ");
	printBoard( );
	Serial.println("Test Over ");
}
void printTestBoard1() {
	// Black
	board[10].occupiedBy = -1;
	board[19].occupiedBy = -1;
	board[28].occupiedBy = -1;

	// White
	board[0].occupiedBy = 1;
	board[9].occupiedBy = 1;
	board[18].occupiedBy = 1;
	board[27].occupiedBy = 1;
	board[37].occupiedBy = 1;
	board[2].occupiedBy = 1;
	board[11].occupiedBy = 1;
	board[20].occupiedBy = 1;
	board[29].occupiedBy = 1;
	board[1].occupiedBy = 1;
	

//	cout << endl << "Test Board 1 " << endl;
	printBoard( );
//	cout << "Test over  " << endl;

//	cout << "test pathfinder" << endl;
	position testPosition;
	updatePosition( &testPosition, 1, 1 );
	capture( &testPosition ,1, -1, 0, 0 ); 
//	cout << endl << "Test Board after pathfind " << endl;
	printBoard( );
//	cout << "Test over  " << endl;
}

			
void printTestBoard2() {
	// Black
	board[10].occupiedBy = -1;
	board[19].occupiedBy = -1;
	board[28].occupiedBy = -1;

	// White
	board[0].occupiedBy = 1;
	board[9].occupiedBy = 1;
	board[18].occupiedBy = 1;
	board[27].occupiedBy = 1;
	board[2].occupiedBy = 1;
	board[11].occupiedBy = 1;
	board[20].occupiedBy = 1;
	board[29].occupiedBy = 1;
	board[1].occupiedBy = 1;
	

//	cout << endl << "Test Board 1 " << endl;
	printBoard( );
//	cout << "Test over  " << endl;

//	cout << "test pathfinder" << endl;
	position testPosition;
	updatePosition( &testPosition, 1, 1 );
	capture( &testPosition ,1, -1, 0, 0 ); 
//	cout << endl << "Test Board after pathfind " << endl;
	printBoard( );
//	cout << "Test over  " << endl;
}
	

int main() {
	init();
	Serial.begin(9600);
	// Tests capture across board on empty spaces for end game
	printTestBoard(); 
	// Tests capture of pieces
	//printTestBoard1();
	//	printTestBoard2();
	
	Serial.end();
	
	return 0;
}

